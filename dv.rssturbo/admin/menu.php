<?php
use Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!Loader::includeModule('dv.rssturbo')) {
    return false;
}

$menu = array(
    'parent_menu' => 'global_menu_marketing',
    'section' => 'dv.rssturbo',
    'sort' => 50,
    'text' => Loc::getMessage('DVRSSTURBO_MENU_TURBO'),
    "title" => Loc::getMessage('DVRSSTURBO_MENU_TURBO'),
    'url' => 'dv.rssturbo_turbo.php?lang=' . LANGUAGE_ID,
    'more_url' => ["dv.rssturbo_trigger.php"],
    'icon' => 'sender_trig_menu_icon',
    "page_icon" => "sender_trig_page_icon"
);

return $menu;