<?php
$MESS['DVRSSTURBO_MODULE_NAME'] = 'RSS Turbo';
$MESS['DVRSSTURBO_MODULE_DESCRIPTION'] = 'Создание Яндекс Турбостраниц с помощью rss файла.';
$MESS["DVRSSTURBO_PARTNER"] = "Дивиер";
$MESS["PARTNER_URI"] = "https://www.divier.ru/";
$MESS['DVRSSTURBO_INSTALL_TITLE'] = 'Установка модуля &laquo;RSS Turbo&raquo;';
$MESS['DVRSSTURBO_UNINSTALL_TITLE'] = 'Удаление модуля &laquo;RSS Turbo&raquo;';