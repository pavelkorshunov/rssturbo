<?php
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (class_exists('dv_rssturbo')) {
	return;
}

class dv_rssturbo extends \CModule
{
	/**
	 * ID модуля
	 * @var string
	 */
	public $MODULE_ID = "dv.rssturbo";

	/**
	 * Версия модуля
	 * @var string
	 */
	public $MODULE_VERSION;

	/**
	 * Дата выхода версии
	 * @var string
	 */
	public $MODULE_VERSION_DATE;

	/**
	 * Название модуля
	 * @var string
	 */
	public $MODULE_NAME;

	/**
	 * Описание модуля
	 * @var string
	 */
	public $MODULE_DESCRIPTION;

	/**
	 * Путь к корню сайта
	 * @var string
	 */
    public $docRoot = '';

    /**
     * Папки которые необходимо установить
     * @var array
     */
    public $installDirs = array(
        'admin' => 'admin',
        'images' => 'dv.rssturbo',
    );

	/**
	 * Конструктор модуля "RSSTurbo"
	 *
	 * @return void
	 */
	public function __construct()
	{
		$arModuleVersion = array();

        $context = \Bitrix\Main\Application::getInstance()->getContext();
        $server = $context->getServer();
        $this->docRoot = $server->getDocumentRoot();

		$path = str_replace('\\', '/', __FILE__);
        $path = substr($path, 0, strlen($path) - strlen('/index.php'));
		include($path . '/version.php');

		if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
			$this->MODULE_VERSION = $arModuleVersion['VERSION'];
			$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		}

		$this->MODULE_NAME = Loc::getMessage('DVRSSTURBO_MODULE_NAME');
		$this->MODULE_DESCRIPTION = Loc::getMessage('DVRSSTURBO_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage("DVRSSTURBO_PARTNER");
        $this->PARTNER_URI = Loc::getMessage("PARTNER_URI");
	}

	/**
	 * Установка модуля
     * @return void
	 */
	public function doInstall()
	{
        global $APPLICATION;

        $this->installFiles();
        registerModule($this->MODULE_ID);

        $APPLICATION->includeAdminFile(
            Loc::getMessage('DVRSSTURBO_INSTALL_TITLE'),
            $this->docRoot . '/local/modules/'. $this->MODULE_ID .'/install/step.php'
        );
	}

    /**
     * Удаление модуля
     * @return void
     */
	public function doUninstall()
	{
        global $APPLICATION;

        $this->uninstallFiles();
        unregisterModule($this->MODULE_ID);

        $APPLICATION->includeAdminFile(
            Loc::getMessage('DVRSSTURBO_UNINSTALL_TITLE'),
            $this->docRoot . '/local/modules/'. $this->MODULE_ID .'/install/unstep.php'
        );
	}

    /**
     * Установка файлов
     * @return boolean
     */
    public function installFiles()
    {
        foreach ($this->installDirs as $dir => $subdir) {
            copyDirFiles(
                $this->docRoot . '/local/modules/'. $this->MODULE_ID .'/install/' . $dir,
                $this->docRoot . '/bitrix/' . $dir,
                true, true
            );
        }

        return true;
    }

    /**
     * Удаление файлов
     * @return boolean
     */
    public function uninstallFiles()
    {
        foreach ($this->installDirs as $dir => $subdir) {
            if ($dir != 'components') {
                deleteDirFilesEx('/bitrix/' . $dir . '/' . $subdir);
            }
        }

        return true;
    }
}